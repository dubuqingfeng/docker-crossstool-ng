#
# About crosstools-ng
#
# version:1.0
FROM ubuntu:14.04
MAINTAINER Dubu Qingfeng <1135326346@qq.com>

# Install by http://wiki.ubuntu.org.cn/Crosstool-ng%E5%88%B6%E4%BD%9C%E4%BA%A4%E5%8F%89%E7%BC%96%E8%AF%91%E5%B7%A5%E5%85%B7%E9%93%BE

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y wget && \
    apt-get install -y build-essential gperf bison flex texinfo gawk libtool libncurses5-dev cvs automake

ENV CT_VERSION 1.21.0

# Install crosstool-NG.

RUN \
    wget http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-${CT_VERSION}.tar.bz2 && \
    tar xf crosstool-ng-*.tar* && \
    cd crosstool-ng-* && \
    ./configure --prefix=/opt/crosstool-ng && make && make install && \
    rm -rf ../crosstool-ng*

ENV PATH="${PATH}:/opt/crosstool-ng/bin"

RUN cd /opt/crosstool-ng/bin

#RUN ./ct-ng build

#COPY samples /home/crosstool-ng

#RUN chmod +x /home/crosstool-ng/samples/build.sh

#CMD ["/home/crosstool-ng/samples/build.sh"]

CMD ["bash"]